#!/usr/bin/env python
# coding: utf-8

# ## Scraping Instagram menggunakan Instaloader

# In[16]
# Library
from instaloader import Instaloader, Profile
import instaloader
from datetime import datetime
from itertools import dropwhile, takewhile
import csv
import pandas as pd
import numpy as np
L = Instaloader()


# In[17]:
# Assign header columns
headerList = ['date', 'username', 'post_type', 'total_likes', 'total_comments','caption', 'url']
  
# Open CSV file and assign header
with open("DatasetScraping.csv", 'w', newline='') as file:
    dw = csv.DictWriter(file, delimiter=',', 
                        fieldnames=headerList)
    dw.writeheader()

# In[18]:
def Login(USER,PASSWORD):
    # login setup and process
    USER = USER
    PASSWORD = PASSWORD
    L.login(USER,PASSWORD)
    
Login(input("Username : "), input("Password : "))

# In[ ]:

# Data yang diambil :
# - Date
# - Username
# - Post Type
# - Likes
# - Comments
# - Caption
# - Date

## Function Get Data
def get_data(username):
    data = []
    df = pd.DataFrame()
    with open('DatasetScraping.csv', 'a', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        posts = instaloader.Profile.from_username(L.context, username).get_posts()
        SINCE = datetime(2022, 6, 6)
        UNTIL = datetime(2022, 6, 12)
        for post in takewhile(lambda p: p.date > SINCE, dropwhile(lambda p: p.date <= UNTIL, posts)):
            post_type = ""
            caption = post.caption
            owner = post.owner_username
            like = post.likes
            com = post.comments
            date = post.date
            url = "https://www.instagram.com/p/" + post.shortcode + "/"
                    
            if (post.typename == 'GraphImage') :
                post_type = "Single"
            elif (post.typename == 'GraphVideo') :
                post_type = "Video"
            elif (post.typename == 'GraphSidecar') :
                post_type = "Carousel"
            else :
                post_type = post.typename
                    
            # add data into list
            temp = [date, owner, like, com, post_type, caption, url]    
            data.append(temp)

            # add data into dataframe
            temp1 = {"date":date, "username":owner, "post_type":post_type, "total_likes":like, "total_comments":com, "caption":caption, "url":url}
            df  = df.append(temp1, ignore_index=True)


            writer.writerow([date, owner, post_type, like, com, caption, url])
            print('Berhasil')
        
    return df
        
df = get_data(input("Akun instagram yang ingin diambil : "))

# ###### Dari 32 data akun instagram, terdapat 3 akun yang hasil scraping tidak muncul dikarenakan data tidak ada dalam rentang Date. 
# Serta beberapa akun tidak muncul hasil scraping tetapi belum tau penyebabnya.

