# ## Scraping Instagram menggunakan Instaloader

# Library
from instaloader import Instaloader, Profile
import instaloader
from datetime import datetime
from itertools import dropwhile, takewhile
import csv
import pandas as pd

# Assign header columns
headerList = ['date', 'username', 'post_type', 'total_likes', 'total_comments','caption', 'url']
L = Instaloader()

# ## Function Login
def Login(USER,PASSWORD):
    # login setup and process
    USER = USER
    PASSWORD = PASSWORD
    L.login(USER,PASSWORD)

# ## Function Get Data
def get_data(USER,PASSWORD,username):
    data = []
    USER = USER
    PASSWORD = PASSWORD
    L.login(USER,PASSWORD)
    with open('DatasetScraping.csv', 'a', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        posts = instaloader.Profile.from_username(L.context, username).get_posts()
        SINCE = datetime(2022, 6, 6)
        UNTIL = datetime(2022, 6, 12)
        for post in takewhile(lambda p: p.date > SINCE, dropwhile(lambda p: p.date <= UNTIL, posts)):
            post_type = ""
            caption = post.caption
            owner = post.owner_username
            like = post.likes
            com = post.comments
            date = post.date
            url = "https://www.instagram.com/p/" + post.shortcode + "/"
                    
            if (post.typename == 'GraphImage') :
                post_type = "Single"
            elif (post.typename == 'GraphVideo') :
                post_type = "Video"
            elif (post.typename == 'GraphSidecar') :
                post_type = "Carousel"
            else :
                post_type = post.typename
                    
            # add data into list
            temp = [date, owner, like, com, post_type, caption, url]    
            data.append(temp)
            writer.writerow([date, owner, post_type, like, com, caption, url])
            print('Berhasil')

# Open CSV file and assign header
with open("DatasetScraping.csv", 'w', newline='') as file:
    dw = csv.DictWriter(file, delimiter=',', fieldnames=headerList)
    dw.writeheader()
            
x = input("Username : ")
y = input("Password : ")
a = input("Akun instagram yang ingin diambil : ")
# print(x)
# print(y)
# print(a)


get_data(x,y,a)


# ###### Dari 32 data akun instagram, terdapat 3 akun yang hasil scraping tidak muncul dikarenakan data tidak ada dalam rentang Date. 
# Serta beberapa akun tidak muncul hasil scraping tetapi belum tau penyebabnya.




