## Scraping Instagram menggunakan Instaloader

# In[16]:
# Library
from instaloader import Instaloader, Profile
import instaloader
from datetime import datetime
from itertools import dropwhile, takewhile
import csv
import pandas as pd
import numpy as np
pd.options.mode.chained_assignment = None  # default='warn'
L = Instaloader()

# In[18]:
def Login(USER,PASSWORD):
    # login setup and process
    USER = USER
    PASSWORD = PASSWORD
    L.login(USER,PASSWORD)
    
Login(input("Username : "), input("Password : "))

# In[ ]:

# Data yang diambil - (7):
# - Date
# - Username
# - Post Type
# - Likes
# - Comments
# - Caption
# - Date

## Function Get Data
def get_data(username):
    data = []
    df = pd.DataFrame()
    posts = instaloader.Profile.from_username(L.context, username).get_posts()
    SINCE = datetime(2022, 6, 6)
    UNTIL = datetime(2022, 6, 12)
    for post in takewhile(lambda p: p.date > SINCE, dropwhile(lambda p: p.date <= UNTIL, posts)):
        post_type = ""
        caption = post.caption
        owner = post.owner_username
        like = post.likes
        com = post.comments
        date = post.date
        url = "https://www.instagram.com/p/" + post.shortcode + "/"
                    
        if (post.typename == 'GraphImage') :
            post_type = "Single"
        elif (post.typename == 'GraphVideo') :
            post_type = "Video"
        elif (post.typename == 'GraphSidecar') :
            post_type = "Carousel"
        else :
            post_type = post.typename

            # add data into dataframe
        temp1 = {"date":date, "username":owner, "post_type":post_type, "total_likes":like, "total_comments":com, "caption":caption, "url":url}
        df  = df.append(temp1, ignore_index=True)
  
    return df
        
df = get_data(input("Akun instagram yang ingin diambil : "))
# In[]
print(df.info())
# print(df.head(3))
# print(df.tail(3))

# In[ ]:
# fungsi buat drop duplicate
def duplicate(df):
    df.drop_duplicates(inplace=True)

# In[]:
# fungsi untuk handling mistaken data type
# headerList = ['date', 'username', 'post_type', 'total_likes', 'total_comments','caption', 'url']
def handleMistake(df):
    # kolom date
    # df['date'] = pd.to_datetime(df['date'],  errors='coerce')
    df['date'] = df['date'].astype(str)
    # kolom username
    df['username'][df['username'].astype(str).str.isdigit()] = np.NaN
    # kolom post_type
    df['post_type'][df['post_type'].astype(str).str.isdigit()] = np.NaN
    # kolom total_likes
    df['total_likes'] = pd.to_numeric(df['total_likes'], errors='coerce')
    # kolom komen
    df['total_comments'] = pd.to_numeric(df['total_comments'], errors='coerce')


# In[]:
# fungsi untuk handling kasus null
# headerList = ['date', 'username', 'post_type', 'total_likes', 'total_comments','caption', 'url']
def handleNull(df):
    # kolom date
    df['date'] = df['date'].fillna(method='ffill').fillna(method='bfill')
    # kolom akun
    df['username'].fillna("undefined", inplace=True)  
    # kolom post_type
    df['post_type'].fillna("undefined", inplace=True)  
    # kolom likes
    df['total_likes'].fillna("-100", inplace=True)
    # kolom komen
    df['total_comments'].fillna("-100", inplace=True) 

# In[]:
duplicate(df)
handleMistake(df)
handleNull(df) 

print(df.head())
# print(df.info())

# In[]:
import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = ["https://spreadsheets.google.com/feeds",'https://www.googleapis.com/auth/spreadsheets',"https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("gspreadintern-ce5d45fde776.json", scope)
client = gspread.authorize(creds)

# In[]:
nama_sheet= input("nama sheetS: ")
spreadsheet = client.open("Instagram-data-scraping")
worksheet = spreadsheet.add_worksheet(nama_sheet, len(df.index), 7)

# In[]:
worksheet.update([df.columns.values.tolist()] + df.values.tolist())
